group = "org.example"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.3.72"
}

repositories {
    jcenter()
    mavenCentral()
}

kotlin {
    linuxX64 {
        compilations.getByName("main") {
            dependencies {
                val coroutinesVer = "1.3.7"
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-native:$coroutinesVer")
            }
            cinterops.create("amqp_client") {
                val amqpClientDir = "${System.getProperty("user.home")}/rabbitmq_c-0.10.0"
                includeDirs("$amqpClientDir/include")
                // TODO: Find a way to set the library path for the linker in this build file instead of hard coding
                //  the absolute path in the amqp_client.def file.
                linkerOpts("-L$amqpClientDir/lib/x86_64-linux-gnu", "-lrabbitmq")
            }
        }
        binaries {
            executable("amqp_client") {
                entryPoint = "org.example.amqpClient.main"
            }
        }
    }
}