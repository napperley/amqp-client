package org.example.amqpClient

import amqpClient.*
import kotlinx.cinterop.*
import platform.posix.fprintf
import platform.posix.stderr
import kotlin.system.exitProcess

@Suppress("EXPERIMENTAL_API_USAGE")
internal class Amqp(val host: String, val port: UInt, val username: String, val password: String) {
    private var conn: amqp_connection_state_t? = null
    private val channel = 1.toUShort()

    @Suppress("RedundantSuspendModifier")
    suspend fun publishMessage(exchange: String, routingKey: String, msgBody: String) {
        memScoped {
            val contentTypePtr = "text/plain".cstr.ptr
            val props = alloc<amqp_basic_properties_t>()
            setupAmqpProperties(props, contentTypePtr)
            println("Publishing message...")
            val status = amqp_basic_publish(
                state = conn,
                channel = channel,
                exchange = amqp_cstring_bytes(exchange.cstr.ptr),
                routing_key = amqp_cstring_bytes(routingKey.cstr.ptr),
                mandatory = false.intValue,
                immediate = false.intValue,
                properties = props.ptr,
                body = amqp_cstring_bytes(msgBody.cstr.ptr)
            )
            if (status != AMQP_STATUS_OK) println("Cannot publish message.")
        }
    }

    private fun setupAmqpProperties(props: amqp_basic_properties_t, contentTypePtr: CPointer<ByteVar>) {
        with(props) {
            // Set the content type.
            amqp_cstring_bytes(contentTypePtr).place(content_type.ptr)
            _flags = (AMQP_BASIC_CONTENT_TYPE_FLAG or AMQP_BASIC_DELIVERY_MODE_FLAG).toUInt()
            // Persistent delivery mode.
            delivery_mode = 2.toUByte()
        }
    }

    private fun openChannel() {
        if (conn != null) {
            println("Opening channel...")
            amqp_channel_open(conn, channel)
            val rpcReply = amqp_get_rpc_reply(conn)
            rpcReply.useContents { handleChannelReplyType(reply_type) }
        }
    }

    private fun handleChannelReplyType(replyType: amqp_response_type_enum) {
        if (replyType != AMQP_RESPONSE_NORMAL) {
            fprintf(stderr, "Cannot open channel.\n")
            exitProcess(-1)
        }
    }

    private fun openSocket() {
        val socketRc = amqp_socket_open(createSocket(), host, port.toInt())
        if (socketRc != 0) {
            fprintf(stderr, "Cannot open TCP socket.\n")
            exitProcess(-1)
        }
    }

    private fun createSocket(): CPointer<amqp_socket_t>? {
        val socket = amqp_tcp_socket_new(conn)
        if (socket == null) {
            fprintf(stderr, "Cannot create TCP socket.\n")
            exitProcess(-1)
        }
        return socket
    }

    private fun login(
        vHost: String = "/",
        maxChannels: Int = AMQP_DEFAULT_MAX_CHANNELS,
        maxFrames: Int = AMQP_DEFAULT_FRAME_SIZE,
        heartBeat: Int = AMQP_DEFAULT_HEARTBEAT,
        saslMethod: amqp_sasl_method_enum = AMQP_SASL_METHOD_PLAIN
    ) = amqp_login(conn, vHost, maxChannels, maxFrames, heartBeat, saslMethod, username.cstr, password.cstr)

    fun connect() {
        println("Logging into AMQP Broker...")
        if (conn == null) {
            conn = amqp_new_connection()
            openSocket()
        }
        val rpcReply = login()
        rpcReply.useContents { handleConnectionReplyType(reply_type) }
        openChannel()
    }

    private fun handleConnectionReplyType(replyType: amqp_response_type_enum) {
        if (replyType != AMQP_RESPONSE_NORMAL) {
            val errorType = when (replyType) {
                AMQP_RESPONSE_SERVER_EXCEPTION -> "server"
                AMQP_RESPONSE_LIBRARY_EXCEPTION -> "library"
                else -> "socket"
            }
            fprintf(stderr, "Cannot login to Broker ($errorType error).\n")
            exitProcess(-1)
        }
    }

    private fun consume(
        queue: String,
        noLocal: Boolean = false,
        noAcknowledgement: Boolean = true,
        exclusive: Boolean = false
    ) = memScoped {
        amqp_basic_consume(
            state = conn,
            channel = channel,
            queue = amqp_cstring_bytes(queue.cstr.ptr),
            consumer_tag = amqp_empty_bytes.readValue(),
            no_local = noLocal.intValue,
            no_ack = noAcknowledgement.intValue,
            exclusive = exclusive.intValue,
            arguments = amqp_empty_table.readValue()
        )
    }

    @Suppress("RedundantSuspendModifier")
    suspend fun fetchMessage(queue: String): AmqpMessage? = memScoped {
        var result: AmqpMessage? = null
        val msg = allocArray<amqp_message_t>(1)
        // Poll for the next message in the queue, and set no_ack to false to prevent the message being
        // removed from the queue.
        val reply = amqp_basic_get(
            queue = amqp_cstring_bytes(queue.cstr.ptr),
            channel = channel,
            state = conn,
            no_ack = false.intValue
        )
        reply.useContents {
            if (reply_type == AMQP_RESPONSE_NORMAL) {
                // Read the message after polling via the amqp_basic_get function.
                amqp_read_message(channel = channel, state = conn, message = msg[0].ptr, flags = 0)
                result = createAmqpMessage(msg[0])
            }
        }
        result
    }

    @Suppress("RedundantSuspendModifier")
    suspend fun consumeMessage(queue: String): AmqpMessage? = memScoped {
        var result: AmqpMessage? = null
        consume(queue)
        val envelope = allocArray<amqp_envelope_t>(1)
        val reply = amqp_consume_message(state = conn, envelope = envelope[0].ptr, timeout = null, flags = 0)
        reply.useContents {
            if (reply_type == AMQP_RESPONSE_NORMAL) {
                result = createAmqpMessage(envelope[0].message)
            }
        }
        amqp_destroy_envelope(envelope)
        result
    }

    fun disconnect() {
        println("Disconnecting from AMQP Broker...")
        if (conn != null) amqp_connection_close(conn, AMQP_REPLY_SUCCESS)
    }

    fun close() {
        disconnect()
        amqp_channel_close(conn, channel, AMQP_REPLY_SUCCESS)
        amqp_destroy_connection(conn)
    }
}
