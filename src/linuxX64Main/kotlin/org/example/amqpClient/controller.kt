@file:Suppress("EXPERIMENTAL_API_USAGE")

package org.example.amqpClient

internal const val EXCHANGE = "amq.topic"
internal var msgCounter = 1

internal val Boolean.intValue: Int
    get() = if (this) 1 else 0
