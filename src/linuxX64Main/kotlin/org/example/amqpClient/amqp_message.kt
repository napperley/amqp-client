package org.example.amqpClient

import amqpClient.amqp_message_t
import kotlinx.cinterop.readBytes
import kotlinx.cinterop.toKString

internal data class AmqpMessage(
    val body: String,
    val contentType: String,
    val contentEncoding: String,
    val appId: String,
    val msgId: String,
    val userId: String,
    val replyTo: String,
    val expiration: String,
    val deliveryMode: Int
)

@ExperimentalUnsignedTypes
internal fun createAmqpMessage(msg: amqp_message_t): AmqpMessage {
    val contentTypeLength = msg.properties.content_type.len.toInt()
    val contentType = msg.properties.content_type.bytes?.readBytes(contentTypeLength)?.toKString() ?: ""
    val contentEncodingLength = msg.properties.content_encoding.len.toInt()
    val contentEncoding = msg.properties.content_encoding.bytes?.readBytes(contentEncodingLength)?.toKString() ?: ""
    val expirationLength = msg.properties.expiration.len.toInt()
    val expiration = msg.properties.expiration.bytes?.readBytes(expirationLength)?.toKString() ?: ""
    return AmqpMessage(
        appId = msg.properties.app_id.bytes?.readBytes(msg.properties.app_id.len.toInt())?.toKString() ?: "",
        userId = msg.properties.user_id.bytes?.readBytes(msg.properties.user_id.len.toInt())?.toKString() ?: "",
        msgId = msg.properties.message_id.bytes?.readBytes(msg.properties.message_id.len.toInt())?.toKString() ?: "",
        contentType = contentType,
        contentEncoding = contentEncoding,
        expiration = expiration,
        replyTo = msg.properties.reply_to.bytes?.readBytes(msg.properties.reply_to.len.toInt())?.toKString() ?: "",
        body = msg.body.bytes?.readBytes(msg.body.len.toInt())?.toKString() ?: "",
        deliveryMode = msg.properties.delivery_mode.toInt()
    )
}
