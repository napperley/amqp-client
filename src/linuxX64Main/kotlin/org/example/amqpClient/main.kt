@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.amqpClient

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

private lateinit var amqp: Amqp

fun main(args: Array<String>) = runBlocking {
    val mode = args.first()
    val username = args[2]
    val password = args[3]
    val host = args.last().split(":").first()
    val port = args.last().split(":").last().toUInt()
    amqp = Amqp(host = host, port = port, username = username, password = password)
    amqp.connect()
    when (mode) {
        "publish" -> runPublisherService(args[1])
        "consume" -> runConsumerService(args[1])
        "fetch" -> runFetchService(args[1])
    }
    println("Exiting...")
    amqp.close()
}

private suspend fun runPublisherService(routingKey: String) = coroutineScope {
    while (true) {
        // Publish a message to the exchange via the routing key (eg echo).
        amqp.publishMessage(exchange = EXCHANGE, routingKey = routingKey, msgBody = "Hello World! :) #$msgCounter")
        msgCounter++
        delay(2000L)
    }
}

private suspend fun runFetchService(queue: String) = coroutineScope {
    println("Fetching messages...")
    while (true) {
        // Do a AMQP "get" to poll for the message via a queue (eg test). Note that messages should be consumed since
        // it doesn't block the main thread unlike polling via AMQP "get".
        val msg = amqp.fetchMessage(queue)
        if (msg != null) printMessage(msg)
        delay(1000L)
    }
}

private suspend fun runConsumerService(queue: String) = coroutineScope {
    println("Consuming messages...")
    while (true) {
        // Consume a message off a queue (eg test).
        val msg = amqp.consumeMessage(queue)
        if (msg != null) printMessage(msg)
        delay(1000L)
    }
}

private fun printMessage(msg: AmqpMessage) {
    println("-- Incoming Message --")
    with(msg) {
        println("Application ID: $appId")
        println("User ID: $userId")
        println("Message ID: $msgId")
        println("Content Type: $contentType")
        println("Content Encoding: $contentEncoding")
        println("Expiration: $expiration")
        println("Reply To: $replyTo")
        println("Delivery Mode: $deliveryMode")
        println("Body: $body")
    }
}
